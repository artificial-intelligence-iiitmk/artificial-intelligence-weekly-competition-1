 # Artificial Intelligence Weekly Competition 1

#### This a weekly test to test the members on their skills in their fields by putting them against each other.

## Welcome to the 1st Weekly Competitions!
### Since this is your first competition make sure you understand the rules of the competitions. 
Note that any submission that does not satisfy the rules will be automatically disqualified. 
## How to Submit/Rules
1.  You can start the competition by creating a fork of the project **Weekly Competition 1 Artificial Intelligence**
2. The submissions should be made inside the folder called **submissions**.
3. The submissions should be made as a **Jupyter Notebook**, under the title **week1.ipynb**. Now since many of you may be new to programming in general just for the first few weeks you can make submissions in a script file. (E.g., week1.py, week1.java, week1.c) 
4. You can use any programming languages you want, but in case of any doubts or queries will be done on programming languages the judges are proficient in. 


## Competition 
### Since this is your first competition here we shall start off with a simple Maze Solving Problem. 

You are Ahana, a helicopter pilot deployed in Nilgiri Biosphere Reserve. One day you get an emergency call saying that one of the trekkers who went that day was trapped in a forest cover and he has lost his way. You immediately fly to the last known position and contact him using the radio. You then spot the trekker stuck in a maze of trees. The trekker requests you to land but you only see an opening to land the helicopter near an opening nearby. It is your responsibility to help the trekker reach the opening by guiding him through the maze. 

---
### Sample Input 
The sample input will consists of a list which contains the following characters  

**"#"**(hastag) : Trees  
**"S"** (alphabet “S”) : Starting Point  
**"_"** (underscore) : Walkable Area  
**"E"** ( alphabet E) : End point  


    [[“#”,“#”,“#”,“#”,“#”,“#”,“#”,“#”,“#”,“#”],[“#”,”S”,“#”,”_”,“#”,”_”, ”_”, ”_”, ”_”,“#”],[ “#”,”_” , “#”,”_”, “#”,”_”, “#”,”_”,”_”, “#”],[“#”,”_”, “#”,“#”,“#”,”_”, “#”,“#”,”_”, “#”],[“#”,”_”,”_”,”_”,”_”,”_”, “#”,”_”,”_”, “#”],[“#”,”_”, “#”,“#”,“#”,”_”, “#”,”_”, “#”,“#”],[“#”,”_”, “#”,”_”, “#”,”_”, “#”,”_”,”_”, “#”],[“#”,”_”,”_”,”_”, “#”,”_”, “#”,”_”,”_”, “#”],[“#”,”_”, “#”,”_”, “#”,”_”, “#”,”_”,”E”, “#”],[“#”,“#”,“#”,“#”,“#”,“#”,“#”,“#”,“#”,“#”]]


The list will denote the top view of the maze. The corresponding view will be something like,





<img src="https://gitlab.com/artificial-intelligence-iiitmk/artificial-intelligence-weekly-competition-1/raw/master/sample%20input.PNG"  width="150" height="200">


### Sample Output
The sample output should consists of outputs which denote the **Direction** in which the trekker should move followed by a blank space and **how much distance to move**.  
In the newline more directions and distances should be printed.  
At last when trekker is adjucent to the End Point **Reached** should be printed.   

    Down 3
    Right 4  
    Up 3  
    Right 3   
    Down 3   
    Left 1  
    Down 4    
    Reached  


The corresponding path taken will look something like this 

<img src="https://gitlab.com/artificial-intelligence-iiitmk/artificial-intelligence-weekly-competition-1/raw/master/sample%20output.PNG"  width="150" height="200">


**Note:**  
There can be different solutions to the same input


### Your submission will be tested through several mazes with starting and end points at random positions and the winner will be decided based on the best submission.   

##### If you have any quires kindly raise an issue using the issue option in the leftmost panel in the GitLab Window or feel free to contact me, Anandha Krishnan H, anandha.mi3@iiitmk.ac.in

## God Speed and May the Force Be With You!


